package com.kcala.ebecshop.common

object GlobalConstants {
    val roundTokenHeaderName = "round_token"
    val defaultUnitName = "piece"
    val defaultPointPerTeam = 2000
}