package com.kcala.ebecshop.common

open class EbecShopException() : RuntimeException() {
    constructor(message: String) : this(){
        RuntimeException(message)
    }
}