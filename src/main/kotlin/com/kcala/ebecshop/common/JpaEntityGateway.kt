package com.kcala.ebecshop.common

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional

abstract class JpaEntityGateway<T> {

    @PersistenceContext
    lateinit var em: EntityManager

    @Transactional
    open fun merge(entity: T) = em.merge(entity)

    @Transactional
    open fun persist(entity: T) = em.persist(entity)

    @Transactional
    open fun find(id: Any): T? = em.find(getEntityClass(), id)

    @Transactional
    open fun remove(entity: T) = em.remove(entity)

    abstract protected fun getEntityClass(): Class<T>


}