package com.kcala.ebecshop.common.delivery.rest

import com.kcala.ebecshop.common.GlobalConstants
import com.kcala.ebecshop.persistRounds.core.boundary.RoundService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
open class RoundFilter : Filter {

    @Autowired
    private lateinit var roundService: RoundService

    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {
        val httpRequest = request as HttpServletRequest
        val httpResponse = response as HttpServletResponse

        val token = httpRequest.getHeader(GlobalConstants.roundTokenHeaderName)
        if (token == null || !token.exist) httpResponse.status = 401
        else chain!!.doFilter(request, response)
    }

    override fun destroy() {
    }

    override fun init(filterConfig: FilterConfig?) {
    }

    private val String.exist: Boolean
        get() = roundService.tokenExists(this)


}
