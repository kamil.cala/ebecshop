package com.kcala.ebecshop.protectFromEdit.core

import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.logTransactions.core.boundary.TransactionService
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.protectFromEdit.core.boundary.EditCheckService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DefaultEditCheckService(@Autowired val transactionService: TransactionService) : EditCheckService {
    override fun isTeamEditable(team: Team) =
            transactionService.getAllForTeam(team).isEmpty()


    override fun isItemEditable(item: Item) =
            transactionService.getAllForItem(item).isEmpty()

}