package com.kcala.ebecshop.protectFromEdit.core.boundary

import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.manageTeams.core.model.Team

interface EditCheckService {
    fun isTeamEditable(team: Team): Boolean
    fun isItemEditable(item: Item): Boolean
}