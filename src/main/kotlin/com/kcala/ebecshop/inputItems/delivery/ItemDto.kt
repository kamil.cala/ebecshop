package com.kcala.ebecshop.inputItems.delivery

import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.persistRounds.core.model.Round

data class ItemDto(
        val id: Int,
        val name: String,
        val pricePerUnit: Int,
        val unit: String,
        val maxPerTeam: Int,
        val barcode: Long,
        val editable: Boolean = true
) {
    fun toItem(round: Round): Item
            = Item.withId(name, pricePerUnit, unit, round, maxPerTeam, barcode, id)
}

fun Item.toDto(editable: Boolean) = ItemDto(id, name, pricePerUnit, unit, maxPerTeam, barcode, editable)