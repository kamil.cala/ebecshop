package com.kcala.ebecshop.inputItems.delivery

import com.kcala.ebecshop.getCurrentRound.core.boundary.CurrentRoundService
import com.kcala.ebecshop.inputItems.core.boundary.ItemService
import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.protectFromEdit.core.boundary.EditCheckService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/items")
class ItemResource(
        @Autowired val itemService: ItemService,
        @Autowired val currentRoundService: CurrentRoundService,
        @Autowired val editCheckService: EditCheckService) {

    @GetMapping
    fun getAllItems(): List<ItemDto>
            = itemService.getAllInRound(currentRoundService.get()).map { it.toDto() }.sortedBy { it.name }

    @PostMapping
    fun createItem(@RequestBody itemDto: ItemDto): ItemDto {
        val item = Item.newItem(
                itemDto.name,
                itemDto.pricePerUnit,
                itemDto.unit,
                currentRoundService.get(),
                itemDto.maxPerTeam,
                itemDto.barcode)
        return itemService.createNewItem(item).toDto()
    }

    @PutMapping(path = arrayOf("{id}"))
    fun editItem(@PathVariable id: Int, @RequestBody itemDto: ItemDto): ItemDto
            = itemService.editItem(id, itemDto.toItem(currentRoundService.get())).toDto()

    @DeleteMapping(path = arrayOf("{id}"))
    fun removeItem(@PathVariable id: Int) {
        val item = itemService.findByIdAndRound(id, currentRoundService.get()) ?: throw ItemService.NoSuchItemException()
        itemService.removeItem(item)
    }

    fun Item.toDto() = toDto(editCheckService.isItemEditable(this))
}