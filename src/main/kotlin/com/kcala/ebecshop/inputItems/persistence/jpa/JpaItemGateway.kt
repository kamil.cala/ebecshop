@file:Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")

package com.kcala.ebecshop.inputItems.persistence.jpa

import com.kcala.ebecshop.common.JpaEntityGateway
import com.kcala.ebecshop.inputItems.core.boundary.ItemGateway
import com.kcala.ebecshop.inputItems.core.boundary.ItemService
import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.persistRounds.core.model.Round
import org.springframework.stereotype.Component
import javax.persistence.NoResultException
import javax.transaction.Transactional

@Component
open class JpaItemGateway : ItemGateway, JpaEntityGateway<Item>() {
    override fun findByIdAndRound(id: Int, round: Round): Item? {
        try {
            return em.createQuery("select i from Item i where i.id = :id and i.round.token = :roundToken", getEntityClass())
                    .setParameter("id", id)
                    .setParameter("roundToken", round.token)
                    .singleResult
        } catch(e: NoResultException) {
            return null
        }
    }

    override fun findByNameAndRound(name: String, round: Round): Item? {
        try {
            return em.createQuery("select i from Item i where i.name = :name and i.round.token = :roundToken", getEntityClass())
                    .setParameter("name", name)
                    .setParameter("roundToken", round.token)
                    .singleResult
        } catch(e: NoResultException) {
            return null
        }
    }

    override fun getAllForRound(round: Round): List<Item> {
        val resultList = em.createQuery("select i from Item i where i.round.token = :roundToken", getEntityClass())
                .setParameter("roundToken", round.token)
                .resultList
        return resultList.toList()
    }

    @Transactional
    override fun removeIfRoundMatches(item: Item) {
        if (em.createQuery("select count(i.id) from Item i where i.id=:id and i.round.token = :roundToken")
                .setParameter("id", item.id)
                .setParameter("roundToken", item.round.token)
                .singleResult as Long > 0)
            removeById(item.id)
        else
            throw ItemService.NoSuchItemException()
    }

    @Transactional
    private fun removeById(id: Int) {
        em.createQuery("delete from Item i where i.id = :id")
                .setParameter("id", id)
                .executeUpdate()
    }

    override fun getEntityClass(): Class<Item> = Item::class.java

}

