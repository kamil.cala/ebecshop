package com.kcala.ebecshop.inputItems.core.boundary

import com.kcala.ebecshop.common.EbecShopException
import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.persistRounds.core.model.Round

interface ItemService {

    fun getAllInRound(round: Round): List<Item>

    fun findByIdAndRound(id: Int, round: Round): Item?

    fun createNewItem(item: Item): Item

    fun editItem(id: Int, item: Item): Item

    fun removeItem(item: Item)

    class NoSuchItemException : EbecShopException()
    class DuplicateItemNameException : EbecShopException()
    class ItemNotRemovableException : EbecShopException()
}