package com.kcala.ebecshop.inputItems.core

import com.kcala.ebecshop.inputItems.core.boundary.ItemGateway
import com.kcala.ebecshop.inputItems.core.boundary.ItemService
import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.manageTeams.core.boundary.TeamService
import com.kcala.ebecshop.persistRounds.core.model.Round
import com.kcala.ebecshop.protectFromEdit.core.boundary.EditCheckService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
open class DefaultItemService(
        @Autowired val itemGateway: ItemGateway,
        @Autowired val editCheckService: EditCheckService) : ItemService {
    override fun getAllInRound(round: Round): List<Item> = itemGateway.getAllForRound(round)

    override fun findByIdAndRound(id: Int, round: Round): Item?
            = itemGateway.findByIdAndRound(id, round)

    override fun createNewItem(item: Item): Item {
        if (item.alreadyExistsWithThisName()) {
            throw ItemService.DuplicateItemNameException()
        }
        itemGateway.persist(item)
        return item
    }

    override fun editItem(id: Int, item: Item): Item {
        throwIfNotEditable(item)
        val existingItem = itemGateway.findByIdAndRound(id, item.round) ?: throw ItemService.NoSuchItemException()
        val modifiedItem = existingItem.copy(
                name = item.name,
                pricePerUnit = item.pricePerUnit,
                unit = item.unit,
                maxPerTeam = item.maxPerTeam,
                barcode = item.barcode)
        if ((existingItem.name != modifiedItem.name) && modifiedItem.alreadyExistsWithThisName()) {
            throw TeamService.DuplicateTeamNameException()
        }
        return itemGateway.merge(modifiedItem)
    }

    override fun removeItem(item: Item) {
        throwIfNotEditable(item)
        itemGateway.removeIfRoundMatches(item)
    }

    private fun throwIfNotEditable(item: Item) {
        if (!editCheckService.isItemEditable(item)) {
            throw ItemService.ItemNotRemovableException()
        }
    }

    private fun Item.alreadyExistsWithThisName()
            = itemGateway.findByNameAndRound(name, round) != null

}

