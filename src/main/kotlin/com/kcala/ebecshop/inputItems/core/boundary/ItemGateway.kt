package com.kcala.ebecshop.inputItems.core.boundary

import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.persistRounds.core.model.Round

interface ItemGateway {
    fun persist(team: Item)

    fun merge(team: Item): Item

    fun findByIdAndRound(id: Int, round: Round): Item?

    fun findByNameAndRound(name: String, round: Round): Item?

    fun getAllForRound(round: Round): List<Item>

    fun removeIfRoundMatches(item: Item)
}