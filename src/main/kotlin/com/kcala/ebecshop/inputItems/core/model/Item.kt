package com.kcala.ebecshop.inputItems.core.model

import com.kcala.ebecshop.persistRounds.core.model.Round
import javax.persistence.*

@Entity
@Table(
        uniqueConstraints = arrayOf(UniqueConstraint(columnNames = arrayOf("name", "round_token")))
)
class Item protected constructor() {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id = 0
        private set
    lateinit var name: String
        private set
    var pricePerUnit: Int = 0
        private set
    lateinit var unit: String
        private set
    var maxPerTeam: Int = 0
        private set
    var barcode: Long = 0
        private set
    @ManyToOne lateinit var round: Round
        private set

    private constructor(name: String,
                        pricePerUnit: Int,
                        unit: String,
                        round: Round,
                        maxPerTeam: Int = 0,
                        barcode: Long = 0,
                        id: Int = 0) : this() {
        this.name = name
        this.pricePerUnit = pricePerUnit
        this.unit = unit
        this.round = round
        this.maxPerTeam = maxPerTeam
        this.barcode = barcode
        this.id = id
    }

    companion object Factory {
        fun newItem(name: String, pricePerUnit: Int, unit: String, round: Round, maxPerTeam: Int, barcode: Long)
                = Item(name, pricePerUnit, unit, round, maxPerTeam, barcode)

        fun withId(name: String, pricePerUnit: Int, unit: String, round: Round, maxPerTeam: Int, barcode: Long, id: Int)
                = Item(name, pricePerUnit, unit, round, maxPerTeam, barcode, id)
    }

    fun copy(name: String = this.name,
             pricePerUnit: Int = this.pricePerUnit,
             unit: String = this.unit,
             round: Round = this.round,
             maxPerTeam: Int = this.maxPerTeam,
             barcode: Long = this.barcode,
             id: Int = this.id)
            = Item(name, pricePerUnit, unit, round, maxPerTeam, barcode, id)

}
