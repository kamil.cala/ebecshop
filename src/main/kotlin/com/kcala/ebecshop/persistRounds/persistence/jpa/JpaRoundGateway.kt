@file:Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")

package com.kcala.ebecshop.persistRounds.persistence.jpa

import com.kcala.ebecshop.common.JpaEntityGateway
import com.kcala.ebecshop.persistRounds.core.boundary.RoundGateway
import com.kcala.ebecshop.persistRounds.core.model.Round
import org.springframework.stereotype.Component
import javax.persistence.NoResultException

@Component
open class JpaRoundGateway : RoundGateway, JpaEntityGateway<Round>() {
    override fun getEntityClass(): Class<Round> {
        return Round::class.java
    }

    override fun findByToken(token: String): Round? {
        try{
            return em.createQuery("select r from Round r where r.token = :token", getEntityClass())
                    .setParameter("token", token)
                    .singleResult
        } catch (e: NoResultException){
            return null
        }
    }


    override fun tokenExists(token: String): Boolean {
        return findByToken(token)!=null
    }
}
