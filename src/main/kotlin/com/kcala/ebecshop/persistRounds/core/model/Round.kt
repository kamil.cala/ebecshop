package com.kcala.ebecshop.persistRounds.core.model

import javax.persistence.Entity
import javax.persistence.Id

@Entity
open class Round protected constructor() {
    lateinit var city: String
        private set
    lateinit var level: RoundLevel
        private set
    var year: Int = 0
        private set
    var pointsPerTeam: Int = 0
        private set
    @Id lateinit var token: String
        private set

    private constructor(city: String, level: RoundLevel, year: Int, pointsPerTeam: Int, token: String) : this() {
        this.city = city
        this.level = level
        this.year = year
        this.pointsPerTeam = pointsPerTeam
        this.token = token
    }

    companion object Factory {
        fun withGeneratedToken(city: String, level: RoundLevel, year: Int, pointsPerTeam: Int)
                = Round(
                city,
                level,
                year,
                pointsPerTeam,
                RoundTokenGenerator.nextRoundToken()
        )

        fun withGivenToken(city: String, level: RoundLevel, year: Int, pointsPerTeam: Int, token: String)
                = Round(city, level, year, pointsPerTeam, token)
    }

    fun copy(city: String = this.city,
             level: RoundLevel = this.level,
             year: Int = this.year,
             pointsPerTeam: Int = this.pointsPerTeam,
             token: String = this.token)
            = Round(city, level, year, pointsPerTeam, token)

    override fun toString(): String {
        return "Round(city='$city', level=$level, year=$year, pointsPerTeam=$pointsPerTeam, token='$token')"
    }

    enum class RoundLevel(val s: String) {
        LOCAL("local"),
        REGION("region"),
        FINAL("final")
    }


}

