package com.kcala.ebecshop.persistRounds.core.boundary

import com.kcala.ebecshop.persistRounds.core.model.Round

interface RoundGateway {
    fun merge(round: Round): Round

    fun persist(round: Round): Unit

    fun findByToken(token: String): Round?

    fun tokenExists(token: String): Boolean
}