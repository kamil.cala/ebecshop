package com.kcala.ebecshop.persistRounds.core.boundary

import com.kcala.ebecshop.persistRounds.core.model.Round

interface RoundService {
    fun createNewRound(round: Round): Round

    fun editRound(token: String, round: Round): Round

    fun findByToken(token: String): Round?

    fun tokenExists(token: String) :Boolean
}

