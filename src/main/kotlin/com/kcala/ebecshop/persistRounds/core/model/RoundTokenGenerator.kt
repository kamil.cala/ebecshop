package com.kcala.ebecshop.persistRounds.core.model

import java.math.BigInteger
import java.security.SecureRandom

internal object RoundTokenGenerator{
    val random: SecureRandom = SecureRandom()

    fun nextRoundToken() = BigInteger(130, random).toString(32)
}