package com.kcala.ebecshop.persistRounds.core

import com.kcala.ebecshop.common.EbecShopException
import com.kcala.ebecshop.persistRounds.core.boundary.RoundGateway
import com.kcala.ebecshop.persistRounds.core.boundary.RoundService
import com.kcala.ebecshop.persistRounds.core.model.Round
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DefaultRoundService(@Autowired val roundGateway: RoundGateway) : RoundService {
    override fun createNewRound(round: Round): Round {
        if (tokenExists(round.token)) {
            throw DuplicateRoundTokenException()
        }
        roundGateway.persist(round)
        return round
    }

    override fun editRound(token: String, round: Round): Round {
        val existingRound = findByToken(token) ?: throw NoSuchRoundException()
        val modifiedRound = existingRound.copy(city = round.city,
                level = round.level,
                year = round.year,
                pointsPerTeam = round.pointsPerTeam)
        return roundGateway.merge(modifiedRound)
    }

    override fun findByToken(token: String): Round? = roundGateway.findByToken(token)


    override fun tokenExists(token: String): Boolean = roundGateway.tokenExists(token)

    class NoSuchRoundException : EbecShopException() {

    }

    class DuplicateRoundTokenException() : EbecShopException("A round with given token already exists") {
    }

}


