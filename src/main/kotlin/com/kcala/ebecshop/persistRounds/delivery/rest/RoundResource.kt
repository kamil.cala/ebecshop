package com.kcala.ebecshop.persistRounds.delivery.rest

import com.kcala.ebecshop.common.EbecShopException
import com.kcala.ebecshop.common.GlobalConstants
import com.kcala.ebecshop.persistRounds.core.boundary.RoundService
import com.kcala.ebecshop.persistRounds.core.model.Round
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/rounds")
class RoundResource(@Autowired val roundsService: RoundService) {

    @GetMapping(path = arrayOf("{token}"))
    fun getByToken(@PathVariable token: String): RoundDto {
        return roundsService.findByToken(token)?.toDto() ?: throw NoSuchRoundException()
    }

    @PostMapping
    fun createNewRound(@RequestBody roundDto: RoundDto): RoundDto {
        val newRound = Round
                .withGeneratedToken(roundDto.city, roundDto.level, roundDto.year, GlobalConstants.defaultPointPerTeam)
        return roundsService.createNewRound(newRound).toDto()
    }

    @PutMapping(path = arrayOf("{token}"))
    fun editRound(@PathVariable token: String, @RequestBody round: RoundDto): RoundDto {
        return roundsService.editRound(token, round.toRound()).toDto()
    }

    class NoSuchRoundException() : EbecShopException()
}