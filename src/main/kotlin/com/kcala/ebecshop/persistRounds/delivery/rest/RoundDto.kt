package com.kcala.ebecshop.persistRounds.delivery.rest

import com.kcala.ebecshop.persistRounds.core.model.Round

data class RoundDto(
        val city: String,
        val level: Round.RoundLevel,
        val year: Int,
        val pointsPerTeam: Int,
        val token: String?
) {
    fun toRound(): Round {
        if (token == null || token.isEmpty()) {
            return Round.withGeneratedToken(city, level, year, pointsPerTeam)
        } else {
            return Round.withGivenToken(city, level, year, pointsPerTeam, token)
        }
    }
}

fun Round.toDto(): RoundDto = RoundDto(city, level, year, pointsPerTeam, token)