package com.kcala.ebecshop.calculatePoints.core.boundary

import com.kcala.ebecshop.manageTeams.core.model.Team

interface PointsService {
    fun getPointsSpentByTeam(team: Team): Int
}