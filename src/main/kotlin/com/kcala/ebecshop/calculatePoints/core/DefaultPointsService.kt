package com.kcala.ebecshop.calculatePoints.core

import com.kcala.ebecshop.calculatePoints.core.boundary.PointsService
import com.kcala.ebecshop.logTransactions.core.boundary.TransactionGateway
import com.kcala.ebecshop.logTransactions.core.model.Transaction
import com.kcala.ebecshop.manageTeams.core.model.Team
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DefaultPointsService(@Autowired val transactionGateway: TransactionGateway): PointsService {
    override fun getPointsSpentByTeam(team: Team): Int {
        val transactions = transactionGateway.findAllForTeam(team)
        return transactions.map(Transaction::totalPrice).sum()
    }

}