package com.kcala.ebecshop.logTransactions.delivery

import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.logTransactions.core.model.Transaction
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Year
import java.time.format.DateTimeFormatter

data class TransactionDto(
        val id: Int = 0,
        val itemId: Int,
        val teamId: Int,
        val quantity: Int,
        val itemName: String = "",
        val price: String = "",
        val teamName: String = "",
        val timestamp: String = ""
) {
    fun toTransaction(round: Round, item: Item, team: Team): Transaction
            = Transaction.withId(round, item, team, quantity, LocalDateTime.now(), id)
}

fun Transaction.toDto(): TransactionDto {
    val price = "${item.pricePerUnit} / ${item.unit}"
    val stamp = timestamp.toSimpleString()
    return TransactionDto(id, item.id, team.id, quantity, item.name, price, team.name, stamp)
}

private fun LocalDateTime.toSimpleString() =
        if (isAfter(LocalDate.now().atStartOfDay()))
            toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm"))
        else if (isAfter(Year.now().atDay(1).atStartOfDay()))
            format(DateTimeFormatter.ofPattern("dd.MM HH:mm"))
        else
            format(DateTimeFormatter.ofPattern("dd.MM.uuuu HH:mm"))
