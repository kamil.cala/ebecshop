package com.kcala.ebecshop.logTransactions.delivery

import com.kcala.ebecshop.getCurrentRound.core.boundary.CurrentRoundService
import com.kcala.ebecshop.logTransactions.core.ItemAndTeamServicesConsumer
import com.kcala.ebecshop.logTransactions.core.boundary.TransactionService
import com.kcala.ebecshop.logTransactions.core.model.Transaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/transactions")
class TransactionResource(
        @Autowired val currentRoundService: CurrentRoundService,
        @Autowired val itemAndTeamServicesConsumer: ItemAndTeamServicesConsumer,
        @Autowired val transactionService: TransactionService
) {

    @GetMapping
    fun getTransactions(
            @RequestParam(value = "team", required = false) teamId: Int?,
            @RequestParam(value = "item", required = false) itemId: Int?): List<TransactionDto> {

        if (teamId != null) {
            return transactionService
                    .getAllForTeam(itemAndTeamServicesConsumer.getTeam(currentRoundService.get(), teamId))
                    .sortedByDescending { it.timestamp }
                    .map(Transaction::toDto)
        }
        if (itemId != null) {
            return transactionService
                    .getAllForItem(itemAndTeamServicesConsumer.getItem(currentRoundService.get(), itemId))
                    .sortedByDescending { it.timestamp }
                    .map(Transaction::toDto)
        }
        return transactionService.getAllInRound(currentRoundService.get())
                .sortedByDescending { it.timestamp }
                .map(Transaction::toDto)
    }


    @PostMapping
    fun logTransaction(@RequestBody transactionDto: TransactionDto): TransactionDto {
        val (item, team) = itemAndTeamServicesConsumer
                .getItemAndTeam(currentRoundService.get(), transactionDto.itemId, transactionDto.teamId)
        val transaction = Transaction.newTransaction(
                currentRoundService.get(),
                item,
                team,
                transactionDto.quantity
        )
        return transactionService.logNewTransaction(transaction).toDto()
    }

    @DeleteMapping("{id}")
    fun deleteTransaction(@PathVariable id: Int)
            = transactionService.removeTransaction(id, currentRoundService.get())
}