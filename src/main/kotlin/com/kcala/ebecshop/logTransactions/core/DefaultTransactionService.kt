package com.kcala.ebecshop.logTransactions.core

import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.logTransactions.core.boundary.TransactionGateway
import com.kcala.ebecshop.logTransactions.core.boundary.TransactionService
import com.kcala.ebecshop.logTransactions.core.model.Transaction
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round
import com.kcala.ebecshop.verifyTransactions.core.boundary.TransactionVerificator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
open class DefaultTransactionService(
        @Autowired val transactionGateway: TransactionGateway,
        @Autowired val transactionVerificator: TransactionVerificator) : TransactionService {

    override fun logNewTransaction(transaction: Transaction): Transaction {
        if (!transactionVerificator.isTransactionValid(transaction)) {
            throw TransactionVerificator.IllegalTransactionException()
        }
        transactionGateway.persist(transaction)
        return transaction
    }

    override fun getAllInRound(round: Round): List<Transaction>
            = transactionGateway.getAllForRound(round)


    override fun findByIdAndRound(id: Int, round: Round): Transaction?
            = transactionGateway.findByIdAndRound(id, round)

    override fun getAllForTeam(team: Team): List<Transaction>
            = transactionGateway.findAllForTeam(team)

    override fun getAllForItem(item: Item): List<Transaction>
            = transactionGateway.findAllForItem(item)

    override fun getAllForTeamAndItem(team: Team, item: Item): List<Transaction>
            = transactionGateway.findAllForTeamAndItem(team, item)

    override fun removeTransaction(id: Int, round: Round) {
        transactionGateway.removeIfRoundMatches(id, round)
    }
}