package com.kcala.ebecshop.logTransactions.core.model

import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round
import java.time.LocalDateTime
import javax.persistence.*

@Entity
class Transaction protected constructor() {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id = 0
        private set
    @ManyToOne lateinit var round: Round
        private set
    @ManyToOne lateinit var item: Item
        private set
    @ManyToOne lateinit var team: Team
        private set
    var quantity: Int = 0
        private set
    lateinit var timestamp: LocalDateTime
        private set

    private constructor(round: Round,
                        item: Item,
                        team: Team,
                        quantity: Int,
                        timestamp: LocalDateTime = LocalDateTime.now(),
                        id: Int = 0) : this() {
        this.round = round
        this.item = item
        this.team = team
        this.quantity = quantity
        this.timestamp = timestamp
        this.id = id
    }

    val totalPrice: Int
        get() = item.pricePerUnit * quantity

    companion object Factory {

        fun newTransaction(round: Round,
                           item: Item,
                           team: Team,
                           quantity: Int,
                           timestamp: LocalDateTime = LocalDateTime.now())
                = Transaction(round, item, team, quantity, timestamp)

        fun withId(round: Round,
                   item: Item,
                   team: Team,
                   quantity: Int,
                   timestamp: LocalDateTime = LocalDateTime.now(),
                   id: Int)
                = Transaction(round, item, team, quantity, timestamp, id)

    }
}

