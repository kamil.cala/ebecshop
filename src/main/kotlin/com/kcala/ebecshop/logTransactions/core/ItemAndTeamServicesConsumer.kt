package com.kcala.ebecshop.logTransactions.core

import com.kcala.ebecshop.inputItems.core.boundary.ItemService
import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.manageTeams.core.boundary.TeamService
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
open class ItemAndTeamServicesConsumer(
        @Autowired val itemService: ItemService,
        @Autowired val teamService: TeamService
) {
    fun getItemAndTeam(round: Round, itemId: Int, teamId: Int): Pair<Item, Team> {
        val item = getItem(round, itemId)
        val team = getTeam(round, teamId)
        return Pair(item, team)
    }

    fun getItem(round: Round, itemId: Int): Item
            = itemService.findByIdAndRound(itemId, round) ?: throw ItemService.NoSuchItemException()

    fun getTeam(round: Round, teamId: Int): Team
            = teamService.findByIdAndRound(teamId, round) ?: throw TeamService.NoSuchTeamException()

}