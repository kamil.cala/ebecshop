package com.kcala.ebecshop.logTransactions.core.boundary

import com.kcala.ebecshop.common.EbecShopException
import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.logTransactions.core.model.Transaction
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round

interface TransactionService {

    fun logNewTransaction(transaction: Transaction): Transaction

    fun getAllInRound(round: Round): List<Transaction>

    fun findByIdAndRound(id: Int, round: Round): Transaction?

    fun getAllForTeam(team: Team): List<Transaction>

    fun getAllForItem(item: Item): List<Transaction>

    fun getAllForTeamAndItem(team: Team, item: Item): List<Transaction>

    fun removeTransaction(id: Int, round: Round)

    class NoSuchTransactionException : EbecShopException()
}