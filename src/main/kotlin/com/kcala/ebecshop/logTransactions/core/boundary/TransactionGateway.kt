package com.kcala.ebecshop.logTransactions.core.boundary

import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.logTransactions.core.model.Transaction
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round

interface TransactionGateway {
    fun persist(transaction: Transaction)

    fun merge(transaction: Transaction): Transaction

    fun findByIdAndRound(id: Int, round: Round): Transaction?

    fun getAllForRound(round: Round): List<Transaction>

    fun removeIfRoundMatches(id: Int, round: Round)

    fun findAllForTeam(team: Team): List<Transaction>

    fun findAllForItem(item: Item): List<Transaction>

    fun findAllForTeamAndItem(team: Team, item: Item): List<Transaction>
}