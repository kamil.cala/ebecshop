@file:Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")

package com.kcala.ebecshop.logTransactions.persistence.jpa

import com.kcala.ebecshop.common.JpaEntityGateway
import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.logTransactions.core.boundary.TransactionGateway
import com.kcala.ebecshop.logTransactions.core.boundary.TransactionService
import com.kcala.ebecshop.logTransactions.core.model.Transaction
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round
import org.springframework.stereotype.Component
import javax.persistence.NoResultException
import javax.transaction.Transactional

@Component
open class JpaTransactionGateway : TransactionGateway, JpaEntityGateway<Transaction>() {
    override fun getEntityClass(): Class<Transaction> = Transaction::class.java

    override fun findByIdAndRound(id: Int, round: Round): Transaction? {
        try {
            return em.createQuery("select t from Transaction t where t.id = :id and t.round.token = :roundToken", getEntityClass())
                    .setParameter("id", id)
                    .setParameter("roundToken", round.token)
                    .singleResult
        } catch(e: NoResultException) {
            return null
        }
    }

    override fun getAllForRound(round: Round): List<Transaction> {
        return em.createQuery("select t from Transaction t where t.round.token = :roundToken", getEntityClass())
                .setParameter("roundToken", round.token)
                .resultList.toList()
    }

    @Transactional
    override fun removeIfRoundMatches(id: Int, round: Round) {
        if (em.createQuery("select count(t.id) from Transaction t where t.id=:id and t.round.token = :roundToken")
                .setParameter("id", id)
                .setParameter("roundToken", round.token)
                .singleResult as Long > 0)
            removeById(id)
        else
            throw TransactionService.NoSuchTransactionException()
    }

    @Transactional
    private fun removeById(id: Int) {
        em.createQuery("delete from Transaction t where t.id = :id")
                .setParameter("id", id)
                .executeUpdate()
    }

    override fun findAllForTeam(team: Team): List<Transaction> {
        return em.createQuery("select t from Transaction t where t.team.id = :teamId", getEntityClass())
                .setParameter("teamId", team.id)
                .resultList.toList()
    }

    override fun findAllForItem(item: Item): List<Transaction> {
        return em.createQuery("select t from Transaction t where t.item.id = :itemId", getEntityClass())
                .setParameter("itemId", item.id)
                .resultList.toList()
    }

    override fun findAllForTeamAndItem(team: Team, item: Item): List<Transaction> {
        return em
                .createQuery("select t from Transaction t where t . team . id = :teamId and t . item . id = :itemId",
                        getEntityClass())
                .setParameter("teamId", team.id)
                .setParameter("itemId", item.id)
                .resultList.toList()
    }
}