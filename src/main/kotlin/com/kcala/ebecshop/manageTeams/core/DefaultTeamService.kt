package com.kcala.ebecshop.manageTeams.core

import com.kcala.ebecshop.manageTeams.core.boundary.TeamGateway
import com.kcala.ebecshop.manageTeams.core.boundary.TeamService
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round
import com.kcala.ebecshop.protectFromEdit.core.boundary.EditCheckService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
open class DefaultTeamService(
        @Autowired val teamGateway: TeamGateway,
        @Autowired val editCheckService: EditCheckService) : TeamService {
    override fun getAllTeamsInRound(round: Round): List<Team> = teamGateway.getAllForRound(round)

    override fun findByIdAndRound(id: Int, round: Round): Team?
            = teamGateway.findByIdAndRound(id, round)

    override fun createNewTeam(team: Team): Team {
        if (team.alreadyExistsWithThisName()) {
            throw TeamService.DuplicateTeamNameException()
        }
        teamGateway.persist(team)
        return team
    }

    override fun editTeam(id: Int, team: Team): Team {
        throwIfNotEditable(team)
        val existingTeam = teamGateway.findByIdAndRound(id, team.round) ?: throw TeamService.NoSuchTeamException()
        val modifiedTeam = existingTeam.copy(name = team.name)
        if ((existingTeam.name != modifiedTeam.name) && modifiedTeam.alreadyExistsWithThisName()) {
            throw TeamService.DuplicateTeamNameException()
        }
        return teamGateway.merge(modifiedTeam)
    }

    override fun removeTeam(team: Team) {
        throwIfNotEditable(team)
        teamGateway.removeIfRoundMatches(team)
    }

    private fun throwIfNotEditable(team: Team) {
        if (!editCheckService.isTeamEditable(team)) {
            throw TeamService.TeamNotRemovableException()
        }
    }

    private fun Team.alreadyExistsWithThisName()
            = teamGateway.findByNameAndRound(name, round) != null

}


