package com.kcala.ebecshop.manageTeams.core.boundary

import com.kcala.ebecshop.common.EbecShopException
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round

interface TeamService {

    fun getAllTeamsInRound(round: Round): List<Team>

    fun findByIdAndRound(id: Int, round: Round): Team?

    fun createNewTeam(team: Team): Team

    fun editTeam(id: Int, team: Team): Team

    fun removeTeam(team: Team)

    class NoSuchTeamException : EbecShopException()
    class DuplicateTeamNameException : EbecShopException()
    class TeamNotRemovableException : EbecShopException()

}