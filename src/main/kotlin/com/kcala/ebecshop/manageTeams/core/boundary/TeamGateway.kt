package com.kcala.ebecshop.manageTeams.core.boundary

import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round

interface TeamGateway {
    fun merge(team: Team): Team

    fun persist(team: Team)

    fun findByIdAndRound(id: Int, round: Round): Team?

    fun findByNameAndRound(name: String, round: Round): Team?

    fun getAllForRound(round: Round): List<Team>

    fun removeIfRoundMatches(team: Team)
}