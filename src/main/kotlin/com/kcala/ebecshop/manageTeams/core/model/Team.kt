package com.kcala.ebecshop.manageTeams.core.model

import com.kcala.ebecshop.persistRounds.core.model.Round
import javax.persistence.*

@Entity
@Table(
        uniqueConstraints = arrayOf(UniqueConstraint(columnNames = arrayOf("name", "round_token")))
)
open class Team protected constructor() {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id = 0
        private set
    lateinit var name: String
        private set
    @ManyToOne lateinit var round: Round
        private set

    private constructor(name: String, round: Round, id: Int = 0) : this() {
        this.name = name
        this.id = id
        this.round = round
    }

    companion object Factory {
        fun newTeam(name: String, round: Round) = Team(name, round)
        fun withId(name: String, round: Round, id: Int) = Team(name, round, id)
    }

    fun copy(name: String = this.name,
             round: Round = this.round,
             id: Int = this.id) = Team(name, round, id)
}