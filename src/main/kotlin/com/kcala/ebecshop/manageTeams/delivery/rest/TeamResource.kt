package com.kcala.ebecshop.manageTeams.delivery.rest

import com.kcala.ebecshop.calculatePoints.core.boundary.PointsService
import com.kcala.ebecshop.getCurrentRound.core.boundary.CurrentRoundService
import com.kcala.ebecshop.manageTeams.core.boundary.TeamService
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.protectFromEdit.core.boundary.EditCheckService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/teams")
class TeamResource(
        @Autowired val teamService: TeamService,
        @Autowired val currentRoundService: CurrentRoundService,
        @Autowired val pointsService: PointsService,
        @Autowired val editCheckService: EditCheckService) {

    @GetMapping
    fun getAllTeams(): List<TeamDto> =
            teamService.getAllTeamsInRound(currentRoundService.get()).map { it.toDto() }.sortedBy { it.name }

    @PostMapping
    fun createTeam(@RequestBody teamDto: TeamDto): TeamDto {
        val team = Team.newTeam(teamDto.name, currentRoundService.get())
        return teamService.createNewTeam(team).toDto()
    }

    @PutMapping(path = arrayOf("{id}"))
    fun editTeam(@PathVariable id: Int, @RequestBody teamDto: TeamDto): TeamDto =
            teamService.editTeam(id, teamDto.toTeam(currentRoundService.get())).toDto()

    @DeleteMapping(path = arrayOf("{id}"))
    fun removeTeam(@PathVariable id: Int) {
        teamService.removeTeam(Team.withId(name = "", id = id, round = currentRoundService.get()))
    }

    private fun Team.toDto(): TeamDto {
        val spent = pointsService.getPointsSpentByTeam(this)
        val left = currentRoundService.get().pointsPerTeam - spent
        val removable = editCheckService.isTeamEditable(this)
        return this.toDto(spent, left, removable)
    }
}

