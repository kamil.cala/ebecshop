package com.kcala.ebecshop.manageTeams.delivery.rest

import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round

data class TeamDto(
        val id: Int,
        val name: String,
        val pointsSpent: Int = 0,
        val pointsLeft: Int = 0,
        val editable: Boolean = true
) {
    fun toTeam(round: Round): Team = Team.withId(this.name, round, this.id)
}

fun Team.toDto(pointsSpent: Int, pointsLeft: Int, editable: Boolean)
        = TeamDto(id, name, pointsSpent, pointsLeft, editable)