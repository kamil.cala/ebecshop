@file:Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")

package com.kcala.ebecshop.manageTeams.persistence.jpa

import com.kcala.ebecshop.common.JpaEntityGateway
import com.kcala.ebecshop.manageTeams.core.boundary.TeamGateway
import com.kcala.ebecshop.manageTeams.core.boundary.TeamService
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round
import org.springframework.stereotype.Component
import javax.persistence.NoResultException
import javax.transaction.Transactional

@Component
open class JpaTeamGatewayJpa : TeamGateway, JpaEntityGateway<Team>() {
    override fun getEntityClass(): Class<Team> = Team::class.java

    override fun findByIdAndRound(id: Int, round: Round): Team? {
        try {
            return em.createQuery("select t from Team t where t.id = :id and t.round.token = :roundToken", getEntityClass())
                    .setParameter("id", id)
                    .setParameter("roundToken", round.token)
                    .singleResult
        } catch(e: NoResultException){
            return null
        }
    }

    override fun findByNameAndRound(name: String, round: Round): Team? {
        try {
            return em.createQuery("select t from Team t where t.name = :name and t.round.token = :roundToken", getEntityClass())
                    .setParameter("name", name)
                    .setParameter("roundToken", round.token)
                    .singleResult
        } catch(e: NoResultException){
            return null
        }
    }


    override fun getAllForRound(round: Round): List<Team> {
        val resultList = em.createQuery("select t from Team t where t.round.token = :roundToken", getEntityClass())
                .setParameter("roundToken", round.token)
                .resultList
        return resultList.toList()
    }

    @Transactional
    override fun removeIfRoundMatches(team: Team) {
        if (em.createQuery("select count(t.id) from Team t where t.id=:id and t.round.token = :roundToken")
                .setParameter("id", team.id)
                .setParameter("roundToken", team.round.token)
                .singleResult as Long > 0)
            removeById(team.id)
        else
            throw TeamService.NoSuchTeamException()
    }

    private fun removeById(id: Int) {
        em.createQuery("delete from Team t where t.id = :id")
                .setParameter("id", id)
                .executeUpdate()
    }
}