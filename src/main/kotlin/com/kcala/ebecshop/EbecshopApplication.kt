package com.kcala.ebecshop

import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.kcala.ebecshop.common.delivery.rest.RoundFilter
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

@SpringBootApplication
open class EbecshopApplication {

    @Bean
    open fun objectMapperBuilder(): Jackson2ObjectMapperBuilder
            = Jackson2ObjectMapperBuilder().modulesToInstall(KotlinModule())

    @Bean
    open fun registerRoundFilter(): FilterRegistrationBean {
        val registrationBean = FilterRegistrationBean()
        registrationBean.filter = getRoundFilter()
        registrationBean.addUrlPatterns("/api/teams/*", "/api/items/*", "/api/transactions/*")
        registrationBean.setName("roundFilter")
        registrationBean.order = 1
        return registrationBean
    }

    @Bean(name = arrayOf("roundFilter"))
    open fun getRoundFilter() = RoundFilter()
}

fun main(args: Array<String>) {
    SpringApplication.run(EbecshopApplication::class.java, *args)
}
