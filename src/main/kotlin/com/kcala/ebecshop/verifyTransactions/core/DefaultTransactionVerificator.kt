package com.kcala.ebecshop.verifyTransactions.core

import com.kcala.ebecshop.calculatePoints.core.boundary.PointsService
import com.kcala.ebecshop.logTransactions.core.boundary.TransactionGateway
import com.kcala.ebecshop.logTransactions.core.model.Transaction
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.verifyTransactions.core.boundary.TransactionVerificator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DefaultTransactionVerificator(
        @Autowired val pointsService: PointsService,
        @Autowired val transactionGateway: TransactionGateway
) : TransactionVerificator {

    override fun isTransactionValid(transaction: Transaction): Boolean
            = !(transaction.exceedsItemsPerTeamLimit() || transaction.exceedsPointsLimit())

    private fun Transaction.exceedsItemsPerTeamLimit()
            = numberOfItemsAlreadySoldForTeam + quantity > item.maxPerTeam

    val Transaction.numberOfItemsAlreadySoldForTeam: Int
        get() = transactionGateway.findAllForTeamAndItem(team, item)
                .map { it.quantity }.sum()


    private fun Transaction.exceedsPointsLimit()
            = team.pointsSpent + totalPrice > round.pointsPerTeam

    val Team.pointsSpent: Int
        get() = pointsService.getPointsSpentByTeam(this)
}