package com.kcala.ebecshop.verifyTransactions.core.boundary

import com.kcala.ebecshop.common.EbecShopException
import com.kcala.ebecshop.logTransactions.core.model.Transaction

interface TransactionVerificator {

    /**
     * Verifies if logging transaction won't exceed team points limit and
     * the maximum items per team limit
     *
     * @param transaction that will be verified
     * @return is transaction valid for being logged
     */
    fun isTransactionValid(transaction: Transaction): Boolean

    class IllegalTransactionException() : EbecShopException()

}
