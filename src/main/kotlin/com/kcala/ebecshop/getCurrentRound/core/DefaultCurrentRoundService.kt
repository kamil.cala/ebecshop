package com.kcala.ebecshop.getCurrentRound.core

import com.kcala.ebecshop.common.EbecShopException
import com.kcala.ebecshop.common.GlobalConstants
import com.kcala.ebecshop.getCurrentRound.core.boundary.CurrentRoundService
import com.kcala.ebecshop.persistRounds.core.boundary.RoundService
import com.kcala.ebecshop.persistRounds.core.model.Round
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.context.annotation.RequestScope
import javax.servlet.http.HttpServletRequest

@RequestScope
@Service
open class DefaultCurrentRoundService(
        @Autowired roundService: RoundService,
        @Autowired request: HttpServletRequest) : CurrentRoundService {

    val currentRound: Round = roundService.findByToken(request.getHeader(GlobalConstants.roundTokenHeaderName))
            ?: throw NoRoundTokenHeaderException()

    override fun get() = currentRound

    class NoRoundTokenHeaderException : EbecShopException() {
    }
}
