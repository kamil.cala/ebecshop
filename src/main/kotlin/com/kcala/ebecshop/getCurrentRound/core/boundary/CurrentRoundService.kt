package com.kcala.ebecshop.getCurrentRound.core.boundary

import com.kcala.ebecshop.persistRounds.core.model.Round

interface CurrentRoundService {
    fun get(): Round
}