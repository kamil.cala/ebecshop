package com.kcala.ebecshop.verifyTransactions.core

import com.kcala.ebecshop.calculatePoints.core.boundary.PointsService
import com.kcala.ebecshop.inputItems.core.model.Item
import com.kcala.ebecshop.logTransactions.core.boundary.TransactionGateway
import com.kcala.ebecshop.logTransactions.core.model.Transaction
import com.kcala.ebecshop.manageTeams.core.model.Team
import com.kcala.ebecshop.persistRounds.core.model.Round
import spock.lang.Specification

import java.time.LocalDateTime

class DefaultTransactionVerificatorTest extends Specification {

    DefaultTransactionVerificator transactionVerificator
    Transaction transaction
    private int MAX_PER_TEAM = 9999

    def setup() {
        prepareVerificator()
        prepareTransaction()
    }

    def "Accepts valid transaction"() {
        expect:
        transactionVerificator.isTransactionValid(transaction)
    }

    def "Refuses transaction that violates points limit"(){
        given: "Transaction too expensive for the team"
        transaction.quantity = 1001
        expect:
        !transactionVerificator.isTransactionValid(transaction)
    }

    def "Refuses transaction that violates items per team limit"(){
        given: "Transaction with too many items"
        transaction.quantity = 10
        transaction.item.maxPerTeam = 5
        expect:
        !transactionVerificator.isTransactionValid(transaction)
    }

    private void prepareTransaction() {
        def round = Round.getField("Factory").get(null).withGeneratedToken("AAA", Round.RoundLevel.LOCAL, 1000, 1000)
        MAX_PER_TEAM = 100
        def item = Item.getField("Factory").get(null).newItem("ITEM", 10, "UUU", round, MAX_PER_TEAM, 0)
        def team = Team.getField("Factory").get(null).newTeam("TEAM", round)
        transaction = Transaction.getField("Factory").get(null).newTransaction(round, item, team, 1, LocalDateTime.now())
    }

    private void prepareVerificator() {
        PointsService pointsService = Stub(PointsService)
        pointsService.getPointsSpentByTeam(_) >> 0
        TransactionGateway transactionGateway = Stub(TransactionGateway)
        transactionGateway.findAllForTeamAndItem(_, _) >> []
        transactionVerificator = new DefaultTransactionVerificator(pointsService, transactionGateway)
    }
}
