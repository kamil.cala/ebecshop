export class Round {
  constructor(public city: String,
              public level: String,
              public year: number,
              public token?: string,
              public pointsPerTeam?: number) {
  }

  static fromObject(obj: any){
    return new Round(obj.city, obj.level, obj.year, obj.token, obj.pointsPerTeam);
  }


}
