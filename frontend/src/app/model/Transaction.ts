export class Transaction {
  constructor(public itemId: number,
              public teamId: number,
              public quantity: number,
              public itemName?: string,
              public price?: string,
              public teamName?: string,
              public timestamp?: string,
              public id?: number) {

  }

  public static fromObject(obj: any) {
    return new Transaction(
      obj.itemId,
      obj.teamId,
      obj.quantity,
      obj.itemName,
      obj.price,
      obj.teamName,
      obj.timestamp,
      obj.id)
  }
}
