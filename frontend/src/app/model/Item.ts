export class Item {
  constructor(public name: string,
              public pricePerUnit?: number,
              public unit?: string,
              public maxPerTeam?: number,
              public barcode?: number,
              public editable?: boolean,
              public id?: number) {
  }

  public static fromObject(obj: any): Item {
    return new Item(obj.name, obj.pricePerUnit, obj.unit, obj.maxPerTeam, obj.barcode, obj.editable, obj.id);
  }

  public static defaultItem(): Item {
    return new Item('', 1, 'piece', 10, 0);
  }
}
