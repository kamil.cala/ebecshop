export class Team {
  constructor(public name: string,
              public pointsSpent?: number,
              public pointsLeft?: number,
              public editable?: boolean,
              public id?: number) {
  }

  static fromObject(obj: any) {
    return new Team(obj.name, obj.pointsSpent, obj.pointsLeft, obj.editable, obj.id);
  }
}

