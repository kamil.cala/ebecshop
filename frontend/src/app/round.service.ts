import {Injectable} from '@angular/core';
import {Round} from './model/Round';
import {Headers, RequestOptions, Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs';
import {EndpointService} from './endpoint.service';
import {RestHelper} from './RestHelper';

@Injectable()
export class RoundService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private options = new RequestOptions({headers: this.headers});

  constructor(private http: Http, private endpoint: EndpointService) {
  }

  public createRound(round: Round): Observable<Response> {
    return this.http.post(this.endpoint.getEndpoint() + 'rounds', round)
      .map(RestHelper.extractData)
      .catch(RestHelper.handleError);
  }

  public getRound(token: string): Observable<Response> {
    return this.http.get(this.endpoint.getEndpoint() + 'rounds/' + token)
      .map(RestHelper.extractData)
      .catch(RestHelper.handleError);
  }

  public updateRound(round: Round): Observable<Round> {
    return this.http.put(this.endpoint.getEndpoint() + 'rounds/' + round.token, round)
      .map(RestHelper.extractData)
      .map((r: any) => Round.fromObject(r))
      .catch(RestHelper.handleError)
  }

}
