import {Injectable} from '@angular/core';
import {Round} from './model/Round';
import {RoundService} from './round.service';
import {Observable} from 'rxjs';

@Injectable()
export class RoundAuthService {

  private round: Round;

  constructor(private roundService: RoundService) {
  }

  getToken(): string {
    if (this.round) {
      return this.round.token;
    }
    return localStorage.getItem('round_token');
  }

  getRound(): Observable<Round> {
    if (this.round) {
      return Observable.of(this.round);
    } else {
      return this.tryGetRoundFromLocalStorage();
    }
  }

  setRound(round: Round) {
    this.round = round;
    localStorage.setItem('round_token', this.round.token);
  }

  updateRound(round: Round) {
    this.roundService.updateRound(round).subscribe(
      round => {
        this.setRound(round)
      }
    )
  }

  deactivateRound() {
    this.round = undefined;
    localStorage.removeItem('round_token');
  }

  tryGetRoundFromLocalStorage(): Observable<Round> {
    var token = localStorage.getItem('round_token');
    if (token == null) {
      return Observable.throw('no token in local storage');
    }
    return this.roundService.getRound(token)
      .map(r => Round.fromObject(r))
      .catch(e => {
        return Observable.throw('wrong token in local storage')
      });
  }

}
