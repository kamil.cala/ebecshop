import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  @Input('text') private alertText: string;
  @Input('status') private alertStatus: string;

  constructor() {
  }

  ngOnInit() {
  }

  private getClasses() {
    return ' ' + 'alert-' + this.alertStatus;
  }

  public info(text: string) {
    this.alertStatus = 'info';
    this.alertText = text;
  }

  public danger(text: string) {
    this.alertStatus = 'danger';
    this.alertText = text;
  }

  public warning(text: string) {
    this.alertStatus = 'warning';
    this.alertText = text;
  }

  public success(text: string) {
    this.alertStatus = 'success';
    this.alertText = text;
  }

}
