import {Injectable} from '@angular/core';
import {RoundAuthService} from './round-auth.service';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {EndpointService} from './endpoint.service';
import {Observable} from 'rxjs';
import {Item} from './model/Item';
import {RestHelper} from './RestHelper';

@Injectable()
export class ItemService {

  private roundToken: string;
  private headers;
  private options;

  constructor(private roundAuthService: RoundAuthService,
              private http: Http,
              private endpoint: EndpointService) {
    this.updateToken();
  }

  private updateToken() {
    this.roundToken = this.roundAuthService.getToken();
    this.headers = new Headers({'round_token': this.roundToken});
    this.options = new RequestOptions({headers: this.headers});
  }

  public createItem(item: Item): Observable<Item> {
    this.updateToken();
    return this.http.post(this.endpoint.getEndpoint() + 'items', item, this.options)
      .map(RestHelper.extractData)
      .map((t: any) => Item.fromObject(t))
      .catch(RestHelper.handleError);
  }

  public getItems(): Observable<Array<Item>> {
    this.updateToken();
    return this.http.get(this.endpoint.getEndpoint() + 'items', this.options)
      .map(RestHelper.extractData)
      .map((data: any) => data.map(t => Item.fromObject(t)))
      .catch(RestHelper.handleError)
  }

  public updateItem(item: Item): Observable<Item> {
    this.updateToken();
    return this.http.put(this.endpoint.getEndpoint() + 'items/' + item.id, item, this.options)
      .map(RestHelper.extractData)
      .map((t: any) => Item.fromObject(t))
      .catch(RestHelper.handleError);
  }

  public removeItem(item: Item): Observable<Response> {
    this.updateToken();
    return this.http.delete(this.endpoint.getEndpoint() + 'items/' + item.id, this.options)
      .catch(RestHelper.handleError);
  }

}
