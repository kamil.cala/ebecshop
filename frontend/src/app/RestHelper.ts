import {Response} from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/Rx';

export class RestHelper{
  constructor(){

  }

  public static handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  public static extractData(res: Response) {
    return res.json();
  }
}
