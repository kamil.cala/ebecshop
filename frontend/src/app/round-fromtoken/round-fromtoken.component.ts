import {Component} from '@angular/core';
import {RoundService} from '../round.service';
import {RoundAuthService} from '../round-auth.service';
import {Router} from '@angular/router';
import {Round} from '../model/Round';

@Component({
  selector: 'app-round-fromtoken',
  templateUrl: './round-fromtoken.component.html',
  styleUrls: ['./round-fromtoken.component.css']
})
export class RoundFromtokenComponent {

  private token: string = '';
  private status = TokenStatus.FRESH;
  private tokenStatus = TokenStatus;

  constructor(private roundService: RoundService,
              private roundAuthService: RoundAuthService,
              private router: Router) {
  }

  onSubmit() {
    this.roundService.getRound(this.token)
      .subscribe(
        resp => {
          this.status = TokenStatus.VALID;
          this.roundAuthService.setRound(Round.fromObject(resp));
          this.router.navigate(['/shop']);
        },
        error => {
          this.status = TokenStatus.INVALID;
        }
      )
  }

}

enum TokenStatus {
  FRESH, INVALID, VALID
}
