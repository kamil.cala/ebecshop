import {Component, OnInit} from '@angular/core';
import {Team} from '../model/Team';
import {TeamService} from '../team.service';
import {RoundAuthService} from '../round-auth.service';

@Component({
  selector: 'app-shop-teams',
  templateUrl: './shop-teams.component.html',
  styleUrls: ['./shop-teams.component.css']
})
export class ShopTeamsComponent implements OnInit {

  private pointsPerTeam = 2000;
  private buttonText = 'Update';
  private newTeamName = '';
  private editedTeamNumber = -1;
  private editedTeamName;

  private teams: Team[] = [];


  constructor(private teamService: TeamService, private roundAuthService: RoundAuthService) {
  }

  ngOnInit() {
    this.updateTeams();
    this.updatePointsPerTeam();
  }

  private updateTeams() {
    this.teamService.getTeams().subscribe(
      teams => this.teams = teams
    )
  }

  private updatePointsPerTeam() {
    this.roundAuthService.getRound().subscribe(
      round => {
        this.pointsPerTeam = round.pointsPerTeam;
      }
    )
  }

  private enableTeamEdit(i: number, team: Team){
    this.editedTeamNumber = i;
    this.editedTeamName = team.name;
  }

  private updateTeam(team: Team) {
    team.name = this.editedTeamName;
    this.teamService.updateTeam(team).subscribe(
      team => {
        this.editedTeamNumber = -1;
        this.updateTeams();
      }
    )

  }

  private removeTeam(team: Team) {
    this.teamService.removeTeam(team).subscribe(
      resp => {
        this.updateTeams();
      }
    )
  }

  private createTeam() {
    this.teamService.createTeam(new Team(this.newTeamName)).subscribe(
      resp => {
        this.newTeamName = '';
        this.updateTeams()
      }
    )
  }

  private editPointsPerTeam() {
    this.roundAuthService.getRound().subscribe(
      round => {
        var newRound = round;
        newRound.pointsPerTeam = this.pointsPerTeam;
        this.roundAuthService.updateRound(newRound);
        setTimeout(() => this.buttonText='Update', 2000);
        this.buttonText = 'Updated';
        this.updatePointsPerTeam();
        this.updateTeams();
      }
    )
  }
}
