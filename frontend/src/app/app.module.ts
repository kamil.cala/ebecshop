import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {RoundComponent} from './round/round.component';
import {ShopComponent} from './shop/shop.component';
import {RoundAuthService} from './round-auth.service';
import {AuthGuardService} from './auth-guard.service';
import {NewRoundComponent} from './new-round/new-round.component';
import {RoundHomeComponent} from './round-home/round-home.component';
import {RoundFromtokenComponent} from './round-fromtoken/round-fromtoken.component';
import {RoundService} from './round.service';
import {EndpointService} from './endpoint.service';
import {ShopSellComponent} from './shop-sell/shop-sell.component';
import {ShopItemsComponent} from './shop-items/shop-items.component';
import {ShopTeamsComponent} from './shop-teams/shop-teams.component';
import {ShopRoundInfoComponent} from './shop-round-info/shop-round-info.component';
import {CapitalizePipe} from './capitalize.pipe';
import {TeamService} from './team.service';
import {DataTableModule} from 'angular2-datatable';
import {ItemService} from './item.service';
import {TransactionService} from './transaction.service';
import {AlertComponent} from './alert/alert.component';


@NgModule({
  declarations: [
    AppComponent,
    RoundComponent,
    ShopComponent,
    NewRoundComponent,
    RoundHomeComponent,
    RoundFromtokenComponent,
    ShopSellComponent,
    ShopItemsComponent,
    ShopTeamsComponent,
    ShopRoundInfoComponent,
    CapitalizePipe,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    DataTableModule,
    RouterModule.forRoot([
      {
        path: 'shop',
        component: ShopComponent,
        canActivate: [AuthGuardService],
        children: [
          {path: 'sell', component: ShopSellComponent},
          {path: 'items', component: ShopItemsComponent},
          {path: 'teams', component: ShopTeamsComponent},
          {path: '', redirectTo: 'sell', pathMatch: 'full'}
        ]
      },
      {
        path: 'round',
        component: RoundComponent,
        children: [
          {path: '', component: RoundHomeComponent},
          {path: 'newround', component: NewRoundComponent},
          {path: 'fromtoken', component: RoundFromtokenComponent}
        ]
      },
      {path: '', redirectTo: '/shop/sell', pathMatch: 'full'}
    ])
  ],
  providers: [RoundAuthService, AuthGuardService, RoundService, EndpointService, TeamService, ItemService, TransactionService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
