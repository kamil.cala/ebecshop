import {Component, OnInit} from '@angular/core';
import {Item} from '../model/Item';
import {ItemService} from '../item.service';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-shop-items',
  templateUrl: './shop-items.component.html',
  styleUrls: ['./shop-items.component.css']
})
export class ShopItemsComponent implements OnInit {

  private items: Item[];
  private editedItemNumber = -1;
  private newItem: Item = Item.defaultItem();
  private editedItem: Item = new Item('');

  constructor(private itemService: ItemService) {
  }

  ngOnInit() {
    this.updateItems();
  }

  private newItemValid(): boolean {
    return this.isItemValid(this.newItem);
  }

  private editedItemValid(): boolean {
    return this.isItemValid(this.editedItem);
  }

  private isItemValid(item: Item): boolean {
    var i: any = item;
    return i.name && i.pricePerUnit && i.unit && i.maxPerTeam && !isNullOrUndefined(i.barcode);
  }

  private updateItems() {
    this.itemService.getItems().subscribe(
      items => this.items = items
    )
  }

  private enableItemEdit(i: number, item: Item) {
    this.editedItemNumber = i;
    this.editedItem = item;
  }

  private updateItem(item: Item) {
    item.name = this.editedItem.name;
    item.pricePerUnit = this.editedItem.pricePerUnit;
    item.unit = this.editedItem.unit;
    item.maxPerTeam = this.editedItem.maxPerTeam;
    item.barcode = this.editedItem.barcode;
    this.itemService.updateItem(item).subscribe(
      item => {
        this.editedItemNumber = -1;
        this.updateItems();
      }
    )
  }

  private createItem() {
    this.itemService.createItem(this.newItem).subscribe(
      resp => {
        this.newItem = Item.defaultItem();
        this.updateItems();
      }
    )
  }

  private removeItem(item: Item){
    this.itemService.removeItem(item).subscribe(
      resp => {
        this.updateItems();
      }
    )
  }

}
