import {Component} from '@angular/core';
import {Round} from '../model/Round';
import {RoundService} from '../round.service';
import {RoundAuthService} from '../round-auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-round',
  templateUrl: './new-round.component.html',
  styleUrls: ['./new-round.component.css']
})
export class NewRoundComponent {
  private currentYear = new Date().getFullYear();
  private years: number[] = [];
  public round: Round = new Round('', 'REGION', this.currentYear);

  constructor(private roundService: RoundService,
              private roundAuthService: RoundAuthService,
              private router: Router) {
    this.fillYearsTable();
  }

  private fillYearsTable() {
    var maxYear: number = this.currentYear + 2;
    for (var i = maxYear; i >= 2000; i--) {
      this.years.push(i)
    }
  }

  onSubmit(): void {
    this.roundService.createRound(this.round).subscribe(
      resp => {
        this.roundAuthService.setRound(Round.fromObject(resp));
        this.router.navigate(['/shop'])
      },
      error => console.log(error)
    );
  }


}
