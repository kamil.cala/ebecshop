import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css', '../../assets/sbadmin2/dist/css/sb-admin-2.css'],
  encapsulation: ViewEncapsulation.None
})
export class ShopComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }


  isRouteActve(route: string): boolean {
    return this.router.isActive(route, true);
  }

}
