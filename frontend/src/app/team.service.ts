import {Injectable} from '@angular/core';
import {RoundAuthService} from './round-auth.service';
import {Team} from './model/Team';
import {Response, Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs';
import {EndpointService} from './endpoint.service';
import {RestHelper} from './RestHelper';

@Injectable()
export class TeamService {

  private roundToken: string;
  private headers;
  private options;

  constructor(private roundAuthService: RoundAuthService,
              private http: Http,
              private endpoint: EndpointService) {
    this.updateToken();
  }

  private updateToken() {
    this.roundToken = this.roundAuthService.getToken();
    this.headers = new Headers({'round_token': this.roundToken});
    this.options = new RequestOptions({headers: this.headers});
  }

  public createTeam(team: Team): Observable<Team> {
    this.updateToken();
    return this.http.post(this.endpoint.getEndpoint() + 'teams', team, this.options)
      .map(RestHelper.extractData)
      .map((t: any) => Team.fromObject(t))
      .catch(RestHelper.handleError);
  }

  public getTeams(): Observable<Array<Team>> {
    this.updateToken();
    return this.http.get(this.endpoint.getEndpoint() + 'teams', this.options)
      .map(RestHelper.extractData)
      .map((data: any) => data.map(t => Team.fromObject(t)))
      .catch(RestHelper.handleError)

  }

  public updateTeam(team: Team): Observable<Team> {
    this.updateToken();
    return this.http.put(this.endpoint.getEndpoint() + 'teams/' + team.id, team, this.options)
      .map(RestHelper.extractData)
      .map((t: any) => Team.fromObject(t))
      .catch(RestHelper.handleError);
  }

  public removeTeam(team: Team): Observable<Response> {
    this.updateToken();
    return this.http.delete(this.endpoint.getEndpoint() + 'teams/' + team.id, this.options)
      .catch(RestHelper.handleError);
  }

}
