import {Injectable} from '@angular/core';

@Injectable()
export class EndpointService {

  constructor() {
  }

  getEndpoint(): string {
    let port = window.location.port;
    return window.location.protocol + '//' + window.location.hostname + ':' + port + '/api/';
  }

}
