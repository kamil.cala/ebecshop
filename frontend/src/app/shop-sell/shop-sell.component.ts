import {Component, OnInit, ViewChild} from '@angular/core';
import {Team} from '../model/Team';
import {Item} from '../model/Item';
import {Transaction} from '../model/Transaction';
import {ItemService} from '../item.service';
import {TeamService} from '../team.service';
import {isNullOrUndefined} from 'util';
import {TransactionService} from '../transaction.service';
import {AlertComponent} from '../alert/alert.component';

@Component({
  selector: 'app-shop-sell',
  templateUrl: './shop-sell.component.html',
  styleUrls: ['./shop-sell.component.css']
})
export class ShopSellComponent implements OnInit {

  private teams: Team[] = [];
  private items: Item[] = [];
  private transactions: Transaction[] = [];

  private selectedTeam: Team;
  private selectedItem: Item;

  private quantity: number = 0;

  @ViewChild(AlertComponent) private alert: AlertComponent;

  constructor(private itemService: ItemService,
              private teamService: TeamService,
              private transactionService: TransactionService) {
  }

  ngOnInit() {
    this.updateItems();
    this.updateTeams();
    this.updateTransactions();
  }

  private updateTeams() {
    this.teamService.getTeams().subscribe(
      teams => {
        this.teams = teams;
        if (this.teams.length > 0 && isNullOrUndefined(this.selectedTeam)) {
          this.selectedTeam = this.teams[0];
        }
      }
    )
  }

  private updateItems() {
    this.itemService.getItems().subscribe(
      items => {
        this.items = items;
        if (this.items.length > 0 && isNullOrUndefined(this.selectedItem)) {
          this.selectedItem = this.items[0];
        }
      }
    )
  }

  private updateTransactions() {
    this.transactionService.getTransactions().subscribe(
      trasactions => {
        this.transactions = trasactions;
      }
    )
  }

  private logTransaction() {
    var transaction: Transaction = new Transaction(this.selectedItem.id, this.selectedTeam.id, this.quantity);
    this.transactionService.logTransaction(transaction).subscribe(
      transaction => {
        this.updateTransactions();
        this.updateTeams();
        this.alert.success('Transaction logged!');
      },
      error => {
        this.alert.danger('Illegal transaction!');
      }
    )
  }

  private revertLast() {
    var transactionToRemove: Transaction = this.transactions[0];
    this.revertTransaction(transactionToRemove);
  }

  private revertTransaction(transaction: Transaction) {
    this.transactionService.deleteTransaction(transaction).subscribe(
      resp => {
        this.alert.info('Transaction removed');
        this.updateTransactions();
        this.updateTeams();
      }
    )
  }

  private sellPossible(): boolean {
    return this.teams.length > 0
      && this.items.length > 0
      && !isNullOrUndefined(this.selectedItem)
      && !isNullOrUndefined(this.selectedTeam)
      && !isNullOrUndefined(this.quantity)
      && this.quantity > 0
  }

  private revertPossible(): boolean {
    return this.transactions.length > 0;
  }
}
