import {Component, OnInit} from '@angular/core';
import {RoundAuthService} from '../round-auth.service';
import {Round} from '../model/Round';
import {Router} from '@angular/router';

@Component({
  selector: 'app-shop-round-info',
  templateUrl: './shop-round-info.component.html',
  styleUrls: ['./shop-round-info.component.css']
})
export class ShopRoundInfoComponent implements OnInit {

  round: Round = new Round('', '', 2000, '', 0);

  constructor(private roundAuthService: RoundAuthService, private router: Router) {
  }

  ngOnInit() {
    this.roundAuthService.getRound().subscribe(
      round => {
        this.round = round
      },
      error => {
        console.log(error)
      }
    )
  }

  private logout() {
    this.roundAuthService.deactivateRound();
    this.router.navigate(['/round'])
  }

}
