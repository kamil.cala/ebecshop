import {Injectable} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {RoundAuthService} from './round-auth.service';
import {Observable} from 'rxjs';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private roundAuthService: RoundAuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.roundAuthService.getRound()
      .map(r => true)
      .catch(() => {
        this.router.navigate(['/round']);
        return Observable.of(false);
      });
  }
}
