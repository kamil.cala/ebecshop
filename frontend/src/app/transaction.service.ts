import {Injectable} from '@angular/core';
import {RequestOptions, Headers, Http, Response} from '@angular/http';
import {EndpointService} from './endpoint.service';
import {RoundAuthService} from './round-auth.service';
import {Transaction} from './model/Transaction';
import {Observable} from 'rxjs';
import {RestHelper} from './RestHelper';

@Injectable()
export class TransactionService {

  private roundToken: string;
  private headers;
  private options;

  constructor(private roundAuthService: RoundAuthService,
              private http: Http,
              private endpoint: EndpointService) {
    this.updateToken();
  }

  private updateToken() {
    this.roundToken = this.roundAuthService.getToken();
    this.headers = new Headers({'round_token': this.roundToken});
    this.options = new RequestOptions({headers: this.headers});
  }

  public logTransaction(transaction: Transaction): Observable<Transaction> {
    this.updateToken();
    return this.http.post(this.endpoint.getEndpoint() + 'transactions', transaction, this.options)
      .map(RestHelper.extractData)
      .map(Transaction.fromObject)
      .catch(RestHelper.handleError);
  }

  public deleteTransaction(transaction: Transaction): Observable<Response> {
    this.updateToken();
    return this.http.delete(this.endpoint.getEndpoint() + 'transactions/' + transaction.id, this.options)
      .catch(RestHelper.handleError);
  }

  public getTransactions(): Observable<Array<Transaction>> {
    this.updateToken();
    return this.http.get(this.endpoint.getEndpoint() + 'transactions', this.options)
      .map(RestHelper.extractData)
      .map((data: any) => data.map(Transaction.fromObject))
      .catch(RestHelper.handleError);
  }

}
